require 'serverspec'
require 'serverspec_extended_types'

set :backend, :exec

RSpec.configure do |c|
  c.before :all do
    c.path = '/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/ec2-user/.local/bin:/home/ec2-user/bin'
  end
end
